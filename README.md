# Task_5_Create_the_python_package

Task_5_Create_the_python_package

Application that takes a string and a file and returns the number of unique characters in the string or a file. 
It is expected that a string with the same character sequence may be passed several times to the method. 
Since the counting operation can be time-consuming, the method should cache the results, 
so that when the method is given a string previously encountered, it will simply retrieve the stored result.
The application has a command-line interface. 
The application has two parameters --string or --file


Homepage: https://git.foxminded.com.ua/foxstudent103298/task_5_create_the_python_package